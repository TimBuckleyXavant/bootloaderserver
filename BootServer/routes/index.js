﻿'use strict';
var express = require('express');
var router = express.Router();
var path = require('path');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Express' });
});

router.get('/boot', function (req, res) {
    res.download(path.join(__dirname, 'boot/boot.hex'), 'boot.hex' );
});

module.exports = router;
